package com.test;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;

import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;

import java.util.ArrayList;
import java.util.Date;

import com.fasterxml.jackson.core.JsonFactory;

import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;

import com.google.api.client.util.Base64;


import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;

import org.apache.http.impl.client.DefaultHttpClient;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class Test {
	
	
	private static String API_KEY = "88b67b0e88e8ae2e346145c9d99ff361";
	private static String PASSWORD = "6ff6c99ffc642f673c2d72ee2c378110";
	
	private static String myShop = "https://testshimano.myshopify.com/admin/collects.json?limit=60";
	
	static final HttpTransport HTTP_TRANSPORT = new NetHttpTransport();
	static final JsonFactory JSON_FACTORY = new JsonFactory();
	
	static String fileName="D:\\Collection_Data.csv";
		
	
	
	//Delimiter used in CSV file
		private static final String COMMA_DELIMITER = ",";
		private static final String NEW_LINE_SEPARATOR = "\n";
		
		//CSV file header
		private static final String FILE_HEADER = "collection_Name,product_Name";
	
static {
	
	

	File f = new File(fileName);
    try {
        BufferedWriter bw = new BufferedWriter(new FileWriter(f, true));
        
    	bw.append(FILE_HEADER);
    	bw.append(COMMA_DELIMITER); 
    	bw.append(NEW_LINE_SEPARATOR);
       
      bw.close();
       
    } catch (IOException e) {
        System.out.println(e.getMessage());
    }
}

		

	//Recursive function that goes through a json object and stores 
	//its key and values in the hashmap 
	 void loadJson(String json) throws ParseException, IOException{
		Long collection_id=null;
		Long product_id=null;
		Long id=null;
		//Parse the JSON data present in the string format
		JSONParser parse = new JSONParser();
		//Type caste the parsed json data in json object
		JSONObject jobj = (JSONObject)parse.parse(json);
		//Store the JSON object in JSON array as objects (For level 1 array element i.e Results)
		JSONArray jsonarr_1 = (JSONArray) jobj.get("collects");
		//Get data for Results array
		ArrayList<FilePOJO> al=new ArrayList<FilePOJO>();
		
		FilePOJO f=null;
		
		
		for(int i=0;i<jsonarr_1.size();i++)
		{
					JSONObject jsonobj_1 = (JSONObject)jsonarr_1.get(i);
					id=(Long) jsonobj_1.get("id");
					
					collection_id=(Long) jsonobj_1.get("collection_id");
					product_id=(Long) jsonobj_1.get("product_id");
					
					String custom_Collection_Tile=custom_Collection_IDMethod(collection_id);
					
					 product_id=(Long) jsonobj_1.get("product_id");
					
					String prod_title=product_IDMethod(product_id);

					if(custom_Collection_Tile.equals(null))
							{
						String smart_Collection_Tile=smart_Collection_IDMethod(collection_id);

						String smartprod_title=product_IDMethod(product_id);
						 f=new FilePOJO();
						f.collection_Name=smart_Collection_Tile;
						f.product_Name=smartprod_title;
						al.add(f);
							}
					else
					{
							
					
		           f=new FilePOJO();
		
					f.collection_Name=custom_Collection_Tile;
					f.product_Name=prod_title;
       
					al.add(f);
					}
        System.out.println("\n  custom_Collection_Tile: " +custom_Collection_Tile);
		
		
		System.out.println("\n  prod_title: " +prod_title);
		
		
		}

		saveDataToFile(al);
		System.out.println("\n Next 60  records");
		againCallToShopify(id);
		
	
		
	}

	
	
	
	
	
	
	public void saveDataToFile(ArrayList<FilePOJO> al) throws IOException
	{
		
		
		File f = new File(fileName);
        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(f, true));
            
        	
            for (FilePOJO filedata : al) {
            	bw.append(filedata.getCollection_Name());
            	bw.append(COMMA_DELIMITER);
            	bw.append(filedata.getProduct_Name());
            	bw.append(NEW_LINE_SEPARATOR);
            	
			}
          bw.close();
           
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }


		
		
	}
	
	
	public void againCallToShopify(Long id) throws ParseException
	{
	System.out.println("againCallToShopify"+id);
	 String myShop_with_SinceId = "https://88b67b0e88e8ae2e346145c9d99ff361:6ff6c99ffc642f673c2d72ee2c378110@testshimano.myshopify.com/admin/collects.json?limit=60&since_id="+id+"";

	try {
		  
		DefaultHttpClient httpClient = new DefaultHttpClient();
			
		String authStr = API_KEY + ":" + PASSWORD;
	        String authEncoded = Base64.encodeBase64String(authStr.getBytes());
	        
			HttpGet getRequest = new HttpGet(myShop_with_SinceId);
			getRequest.setHeader("Authorization", "Basic " + authEncoded);
			
			//getRequest.addHeader("accept", "application/json");
	 
			//System.out.println(getRequest.getURI().toString());
							
			org.apache.http.HttpResponse response = httpClient.execute(getRequest);
	 
							
			if (response.getStatusLine().getStatusCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : "
				   + response.getStatusLine().getStatusCode());
			}
	 
			BufferedReader br = new BufferedReader(
	                         new InputStreamReader((response.getEntity().getContent())));
	 
			String output;
		//	System.out.println("Output from Server .... \n");
			while ((output = br.readLine()) != null) {
				
				
				System.out.println(output);
				
					ShopifyAPI t1=new ShopifyAPI();
				t1.loadJson(output);
				
			}
	 
			httpClient.getConnectionManager().shutdown();
	 
		  } catch (ClientProtocolException e) {
	 
			e.printStackTrace();
	 
		  } catch (IOException e) {
	 
			e.printStackTrace();
		  }
	
	
	
		
	}
	
	
	public String custom_Collection_IDMethod(Long collection_id) throws ParseException
	{
		
		String custom_collection_title=null;
		try {
			
			String myShopwithtitle = "https://testshimano.myshopify.com/admin/custom_collections/"+collection_id+".json?title";
			
			//String myShopwithtitle = "https://testshimano.myshopify.com/admin/custom_collections/2379700476.json?title";
			
			DefaultHttpClient httpClient = new DefaultHttpClient();
				
			String authStr = API_KEY + ":" + PASSWORD;
		        String authEncoded = Base64.encodeBase64String(authStr.getBytes());
		        
				HttpGet getRequest = new HttpGet(myShopwithtitle);
				getRequest.setHeader("Authorization", "Basic " + authEncoded);
				
				//getRequest.addHeader("accept", "application/json");
		 
				//System.out.println(getRequest.getURI().toString());
								
				org.apache.http.HttpResponse response = httpClient.execute(getRequest);
		 
								
				if (response.getStatusLine().getStatusCode() != 200) {
					throw new RuntimeException("Failed : HTTP error code : "
					   + response.getStatusLine().getStatusCode());
				}
		 
				BufferedReader br = new BufferedReader(
		                         new InputStreamReader((response.getEntity().getContent())));
				
				
				
		 String output;
		
		 while ((output = br.readLine()) != null) {
			 
			
				
			custom_collection_title=custom_Collection_Tile(output);
			
			
			
			
     }
		 
 
		httpClient.getConnectionManager().shutdown();
			 
			  } catch (ClientProtocolException e) {
		 
				e.printStackTrace();
		 
			  } catch (IOException e) {
		 
				e.printStackTrace();
			  }
		return custom_collection_title;
	}


	
	
	
	
	
	public String smart_Collection_IDMethod(Long smart_collection_id)throws ParseException
	{
		
		String smart_Collection=null;
		
		try {
			
			String myShopwithtitle = "https://testshimano.myshopify.com/admin/smart_collections/"+smart_collection_id+".json?title";
			 
			DefaultHttpClient httpClient = new DefaultHttpClient();
				
			String authStr = API_KEY + ":" + PASSWORD;
		        String authEncoded = Base64.encodeBase64String(authStr.getBytes());
		        
				HttpGet getRequest = new HttpGet(myShopwithtitle);
				getRequest.setHeader("Authorization", "Basic " + authEncoded);
				
				//getRequest.addHeader("accept", "application/json");
		 
				//System.out.println(getRequest.getURI().toString());
								
				org.apache.http.HttpResponse response = httpClient.execute(getRequest);
		 
								
				if (response.getStatusLine().getStatusCode() != 200) {
					throw new RuntimeException("Failed : HTTP error code : "
					   + response.getStatusLine().getStatusCode());
				}
		 
				BufferedReader br = new BufferedReader(
		                         new InputStreamReader((response.getEntity().getContent())));
		 
				String output=null;
			
				
				
				while ((output = br.readLine()) != null) {
					
					smart_Collection=smart_Collection_Tile(output);
					
				}
		 
				httpClient.getConnectionManager().shutdown();
		 
			  } catch (ClientProtocolException e) {
		 
				e.printStackTrace();
		 
			  } catch (IOException e) {
		 
				e.printStackTrace();
			  }
		return smart_Collection;
	}
	
	
	
	
	
	
	
	
	
	public String product_IDMethod(Long product_id) throws ParseException
	{
		
		String prod_title=null;
		try {
			
			String myShopwithtitle = "https://testshimano.myshopify.com/admin/products/"+product_id+".json?title";
			 
			DefaultHttpClient httpClient = new DefaultHttpClient();
				
			String authStr = API_KEY + ":" + PASSWORD;
		        String authEncoded = Base64.encodeBase64String(authStr.getBytes());
		        
				HttpGet getRequest = new HttpGet(myShopwithtitle);
				getRequest.setHeader("Authorization", "Basic " + authEncoded);
				
				//getRequest.addHeader("accept", "application/json");
		 
				//System.out.println(getRequest.getURI().toString());
								
				org.apache.http.HttpResponse response = httpClient.execute(getRequest);
		 
								
				if (response.getStatusLine().getStatusCode() != 200) {
					throw new RuntimeException("Failed : HTTP error code : "
					   + response.getStatusLine().getStatusCode());
				}
		 
				BufferedReader br = new BufferedReader(
		                         new InputStreamReader((response.getEntity().getContent())));
		 
				String output;
				//System.out.println("Output from Server .... \n");
				while ((output = br.readLine()) != null) {
					
					
								prod_title=product_Tile(output);
					
				}
		 
				httpClient.getConnectionManager().shutdown();
		 
			  } catch (ClientProtocolException e) {
		 
				e.printStackTrace();
		 
			  } catch (IOException e) {
		 
				e.printStackTrace();
			  }
		return prod_title;
	}

	
	public String smart_product_IDMethod(Long product_id) throws ParseException
	{
		
		String smart_prod_title=null;
		try {
			
			String myShopwithtitle = "https://testshimano.myshopify.com/admin/products/"+product_id+".json?title";
			 
			DefaultHttpClient httpClient = new DefaultHttpClient();
				
			String authStr = API_KEY + ":" + PASSWORD;
		        String authEncoded = Base64.encodeBase64String(authStr.getBytes());
		        
				HttpGet getRequest = new HttpGet(myShopwithtitle);
				getRequest.setHeader("Authorization", "Basic " + authEncoded);
				
				//getRequest.addHeader("accept", "application/json");
		 
				//System.out.println(getRequest.getURI().toString());
								
				org.apache.http.HttpResponse response = httpClient.execute(getRequest);
		 
								
				if (response.getStatusLine().getStatusCode() != 200) {
					throw new RuntimeException("Failed : HTTP error code : "
					   + response.getStatusLine().getStatusCode());
				}
		 
				BufferedReader br = new BufferedReader(
		                         new InputStreamReader((response.getEntity().getContent())));
		 
				String output;
				//System.out.println("Output from Server .... \n");
				while ((output = br.readLine()) != null) {
					
					
					smart_prod_title=smart_product_Tile(output);
					
				}
		 
				httpClient.getConnectionManager().shutdown();
		 
			  } catch (ClientProtocolException e) {
		 
				e.printStackTrace();
		 
			  } catch (IOException e) {
		 
				e.printStackTrace();
			  }
		return smart_prod_title;
	}


	
private String custom_Collection_Tile(String custom_col_title) throws ParseException{
	String custom_collection=null;
	
	org.json.JSONObject obj = new org.json.JSONObject(custom_col_title);
   
	custom_collection = obj.getJSONObject("custom_collection").getString("title");

		
		return custom_collection;
	}
	

private String smart_Collection_Tile(String smart_col_title) throws ParseException{
	String smart_collection=null;
	
	org.json.JSONObject obj = new org.json.JSONObject(smart_col_title);
   
	smart_collection = obj.getJSONObject("smart_collection").getString("title");

		
		return smart_collection;
	}






private String product_Tile(String productl_title) throws ParseException{
String custom_product=null;

org.json.JSONObject obj = new org.json.JSONObject(productl_title);

custom_product = obj.getJSONObject("product").getString("title");

	
	return custom_product;
}


private String smart_product_Tile(String productl_title) throws ParseException{
String smart_product=null;

org.json.JSONObject obj = new org.json.JSONObject(productl_title);

smart_product = obj.getJSONObject("product").getString("title");

	
	return smart_product;
}

	
		
	
	
	
	

public static void main(String[] args) throws ParseException  {
	try {
		  
		DefaultHttpClient httpClient = new DefaultHttpClient();
			
		String authStr = API_KEY + ":" + PASSWORD;
	        String authEncoded = Base64.encodeBase64String(authStr.getBytes());
	        
			HttpGet getRequest = new HttpGet(myShop);
			getRequest.setHeader("Authorization", "Basic " + authEncoded);
			
			//getRequest.addHeader("accept", "application/json");
	 
			//System.out.println(getRequest.getURI().toString());
							
			org.apache.http.HttpResponse response = httpClient.execute(getRequest);
	 
							
			if (response.getStatusLine().getStatusCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : "
				   + response.getStatusLine().getStatusCode());
			}
	 
			BufferedReader br = new BufferedReader(
	                         new InputStreamReader((response.getEntity().getContent())));
	 
			String output;
		//	System.out.println("Output from Server .... \n");
			while ((output = br.readLine()) != null) {
				
				
				System.out.println(output);
				
					Test t=new Test();
				t.loadJson(output);
				
			}
	 
			httpClient.getConnectionManager().shutdown();
	 
		  } catch (ClientProtocolException e) {
	 
			e.printStackTrace();
	 
		  } catch (IOException e) {
	 
			e.printStackTrace();
		  }
}



}